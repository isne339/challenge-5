#include<iostream>
#include<string>

using namespace std;

void input_phase(); //Input phase function

int main()
{
	input_phase();	//do input_phase function
	return 0;
}

void input_phase()
{
	string phase,re_phase,cap_phase,del_sym = "1234567890* .+-/\,;:'?&^%$#@!-_()<>="; //symbol string collector
	int size,sym_size;
	cout<<"Enter your phase(enter ^z to end) : ";
	while(getline(cin,phase)) //getline until input ^Z
	{
		cap_phase = phase;
		size = phase.size(); //count size of string contain in size (string function)
		sym_size = del_sym.size(); //count size of string contain in symbol size
		for(int i=0; i<size; i++)
		{
			for(int j=0; j<sym_size; j++)
			{
				while(phase[i]==del_sym[j]) //check slot and symbol
				{
					phase.erase(i,1); //check if slot is one of del_sym (symbol) delete that slot
				}
			}
			if(phase[i]>=97 && phase[i]<=122) //97-122 are Ascii number of a-z 
			{
				phase[i] = toupper(phase[i]);
			}
	 	}
		
		re_phase = string(phase.rbegin(), phase.rend()); //re arange phase : string function
		if (phase == re_phase )
		{
			re_phase = string(cap_phase.rbegin(), cap_phase.rend());
	   	 	cout <<cap_phase<<" Equal to "<< re_phase << " : Palindrome\n";
		}
		else if(phase != re_phase )
		{
			re_phase = string(cap_phase.rbegin(), cap_phase.rend());
			cout <<cap_phase<<" Not equal to "<< re_phase << " : Not Palindrome\n";
		}
		cout<<"Enter your phase(enter ^z to end) : ";
	}	
}
